# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : chatroom

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://chatroom-574b0.web.app/

## Website Detail Description
此聊天室功能包含:
1. 帳號相關：創建帳號、一般登入登出
2. 修改個人資料：可修改使用者名稱、生日
3. 聊天室相關：創建私人聊天室、邀請其他用戶、即時發送訊息、顯示訊息
4. RWD：介面會自動根據頁面大小調整
5. 第三方登入：支援 google 登入
6. Chrome 通知：當收到他人房間邀請或收到新訊息時，會跳出提醒欄
6. CSS 動畫：登入頁面背景為流動式動畫
7. Security Report：當訊息為 html code 時，仍然可以傳送，且顯示正常，不會更改到現有的 html code

<p>1. 登入前頁面</p>
<img src="01.PNG" width='400'></img>
<p>2. 前往登入頁面按紐</p>
<img src="04.PNG" width='400'></img>
<p>3. 登入頁面</p>
<img src="02.PNG" width='400'></img>
<p>4. 登入後頁面</p>
<img src="03.PNG" width='400'></img>
<p>5. 修改個人資料與登出按鈕</p>
<img src="mark01.PNG" width='400'></img>
<p>6. 修改名字與生日頁面</p>
<img src="mark03.PNG" width='400'></img>
<p>7. 創建房間與選擇已加入房間</p>
<img src="mark02.PNG" width='400'></img>
<p>8. 房間內頁面</p>
<img src="05.PNG" width='400'></img>

# Components Description : 

1. 創建帳號 : 可透過在登入畫面輸入信箱與密碼，並按下 'New Account' 來創建帳號
2. 登入 : 可透過輸入已創建帳號的信箱與密碼，並按下 'Sign in' 來進行登入
3. google 登入 : 按下 'Sign in with Google' 後，可由跳出視窗來登入 google 帳號
4. 修改個人名稱與生日 : 用戶名預設為用戶信箱，點擊 'Account' -> '信箱' 後可進入修改資料頁面，在頁面中可輸入想更換的名稱與生日，確認後傳送訊息的名稱會變為輸入名稱，但信箱維持不變
5. 登出 : 點擊 'Account' -> 'Logout' 後即可登出，房間訊息等等都會清空
6. 創建房間 : 點擊 'room' -> 'create new room' 後會跳出視窗，輸入房間名稱即可完成創建
7. 選擇房間 : 所有用戶擁有存取權的房間都會列在 'create new room' 下方，點擊後即可進入該房間，同時聊天室內容會被印出
8. 邀請 : 按下 'invite' 後會跳出視窗，輸入想邀請的用戶信箱後即可完成邀請，成功後會在聊天室內顯示邀請訊息，對方也會收到 chrome 邀請通知
9. 傳送訊息 : 在聊天室內的輸入框輸入訊息後，按下 'submit' 即可傳送訊息，聊天室內容會即時更新，其他於房間中的用戶會收到 chrome 訊息通知

# Other Functions Description : 

1. 重複邀請修正 : 如果邀請已在房間內的用戶，會告知已經加入，並取消邀請
2. 訊息欄位高度調整 : 若發送的訊息有包含換行，印出時會自動將訊息欄位調整到與行數相同，且可用滑鼠拉動訊息欄右下角來手動調整高度
