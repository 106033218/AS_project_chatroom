function init() {
    var ID = '';
    var name = '';
    var date = '';
    var inf;
    firebase.database().ref('users').once('value').then(function(snapshot) {
        ID = snapshot.val().now;
        console.log(ID);
    }).then(function(){
        inf = firebase.database().ref('users/'+ID);
    }); 
    
    
    var inputName = document.getElementById('inputName');
    var inputBirth = document.getElementById('inputBirth');
    var confirm = document.getElementById('btnConfirm')
    var cancel = document.getElementById('btnCancel');
    confirm.addEventListener('click', function() {
        name = inputName.value;
        date = inputBirth.value;
        console.log(name,date);
        if(name==''||date=='') alert('請輸入完整資料!');
        else{
            inf.update({name: name, birthday: date}); 
            window.location.href = "index.html";
        }
    });
    cancel.addEventListener('click', function() {
        window.location.href = "index.html";
    });
}

window.onload = function() {
    init();
};