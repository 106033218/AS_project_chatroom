var room_base={                                                         //負責顯示舊訊息的主要物件
    postsRef : '',
    total_post : [],
    first_count : 0,
    second_count : 0,
}

var room_list = firebase.database().ref('rooms');
var user_room = '';
var user_ID;
var Name;
var notmy = 1;
var notmymes = 1;

var total_room = [];                                                    //用來組合index頁面的新元素
var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
var str_after_content = "</p></div></div>\n";


////////////////////////////////////////////////////////////////////////////////////

function init() {
    
    var user_email = '';
    var user_name = '';
    
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        User = user;
        // Check user login
        if (user) {
            user_email = user.email;
            user_ID = user.uid;
            console.log(user_ID + " login!");
            
            user_room = firebase.database().ref('users/'+user_ID); 
            user_room.update({user : user_email});                                  //把使用者的信箱寫到uid節點裡
            user_room.update({uid : user_ID});
            user_room = firebase.database().ref('users');
            user_room.update({now : user_ID});
            
            var user_base={                                                         //負責顯示房間的主要物件
                postsRef : firebase.database().ref('users/'+user_ID),
                total_post : [],
                first_count : 0,
                second_count : 0,
            }
        
            user_base.postsRef.once('value').then(function(snapshot) {              //把現有聊天室按鈕秀在navbar上
        
                snapshot.forEach(function(childshot) {
                    // console.log(user_base.first_count,user_base.second_count);
                    var childData = childshot.val();
                    if(childData.theName!=undefined){
                        var click = ['onclick="change_room(',"'",childData.theName,"'",')"'];
                        user_base.total_post[user_base.total_post.length] =  '<span class="dropdown-item room_list" id="' + childData.theName + '"' + click.join('') + '>' +childData.theName+'</span>';
                    }
                    user_base.first_count += 1;
                });
        
                document.getElementById('room_list').innerHTML = user_base.total_post.join('');
        
                user_base.postsRef.on('child_added', function(data) {
                    // console.log(user_base.first_count,user_base.second_count);
                    user_base.second_count += 1;
                    if (user_base.second_count > user_base.first_count) {
                        var child_Data = data.val();
                        var _click = ['onclick="change_room(',"'",child_Data.theName,"'",')"'];
                        user_base.total_post[user_base.total_post.length] =  '<span class="dropdown-item room_list" id="' + child_Data.theName + '"' + _click.join('') + '>' +child_Data.theName+'</span>';
                        document.getElementById('room_list').innerHTML = user_base.total_post.join('');
                        if(notmy){
                            notifyMe('您被邀請加入 '+ child_Data.theName +' 聊天室');
                        }
                        notmy = 1;
                    }
                });
                
            })
            .catch(e => console.log(e.message));
            
            firebase.database().ref('users/'+user_ID).once('value').then(function(snapshot) {
                user_name = snapshot.val().name;
                if(user_name==undefined) user_name = user_email;
            });

            menu.innerHTML = "<span class='dropdown-item' id='user_inf'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var user_inf = document.getElementById('user_inf');
            user_inf.addEventListener('click', function() {
                window.location.href = "inf.html";
            });

            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("成功登出!");
                }).catch(function(error) {
                    alert("登出失敗!");
                })
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
            document.getElementById('room').innerHTML = "";
            document.getElementById('invite').innerHTML = "";
            document.getElementById('room_name').innerHTML = '登入後才可使用!';
            // window.location.href = "signin.html";
        }
    });

    


    var post_btn = document.getElementById('post_btn');
    var post_txt = document.getElementById('comment');
    
    
    
    post_btn.addEventListener('click', function() {                         //按下"submit"，把訊息傳到com_list上
        if (post_txt.value != "") {
            var data = {
                data: post_txt.value,
                email: user_email,
                name: user_name
            };
            room_base.postsRef.push(data);
            post_txt.value = "";                                            //清空填寫欄位
            notmymes = 0;
            console.log(notmymes,-1);
        }
    });


    var room = document.getElementById("newroom");                          //create_room按鈕，按下後可創新房間

    room.addEventListener('click', function() {
        Name = window.prompt("請輸入房間名稱","test-room");

        if(Name!='' && Name!=null){
            
            var check = true;
            
            room_list.once('value').then(function(snapshot) {                 //檢查房間是否已存在
                snapshot.forEach(function(childshot) {
                    var childData = childshot.val();
                    if(childData.theName==Name){check=false; console.log('find!',check);}
                });
            })
            .then(function(){
                if(check==true){
                    alert('創立成功! 房間名為: '+Name)
                    
                    notmy = 0;

                    user_room = firebase.database().ref('users/'+user_ID);
                    var theName = {
                        theName: Name
                    };
                    user_room.push(theName);
                    room_list.push(theName);

                    change_room(Name);
                     
                }
                else{
                    alert("此房間名已被註冊，請重新輸入!");
                }
            }).catch(e => console.log(e.message));
        }
        
    });

    var invite = document.getElementById("invite");
    invite.addEventListener('click', function(){
        var guest = window.prompt("請輸入受邀者信箱","");
        var find_check = false;
        var uid = '';
        if(guest!="" && guest!=null){
            var find = firebase.database().ref('users');
            find.once('value').then(function(snapshot) {                 
                snapshot.forEach(function(childshot) {
                    var childData = childshot.val();
                    if(childData.user==guest){
                        find_check=true; 
                        console.log('find!',uid);
                        uid =  childData.uid;
                        var allow = firebase.database().ref('users/'+uid);
                        
                        var check = 0;
                        allow.once('value').then(function(snapshot){
                            snapshot.forEach(function(childshot){
                                var temp = childshot.val();
                                if(temp.theName==Name) check = 1;
                            });
                            console.log(check);
                            if(check==1){alert("此使用者已加入聊天室");}
                            else{
                                var theName = {
                                    theName: Name
                                };
                                allow.push(theName);
                                var postsRef = firebase.database().ref("com_list/"+Name);
                                var data = {
                                    data: '已邀請 '+ guest +' 進入聊天室',
                                    email: '邀請',
                                    name: ''
                                };
                                postsRef.push(data);
                            }
                        });
                        
                    }
                    // else{}
                });
                if(find_check==false){
                    alert("您輸入的帳號不存在或未註冊，請重新輸入!");
                }

            })
        }

    });
    
}

window.onload = function() {
    init();
};

///////////////////////////////////////////////////////////////

function change_room(theName){                                          //換房間時，將所有過去訊息印出
    // room_base.postsRef.off();                                        //關閉上一個房間資料庫的監聽器
    Name = theName;
    room_base = {
        postsRef : firebase.database().ref("com_list/"+theName),        //更新房間資料庫位址
        total_post : [],                                                //清空暫存欄位
        first_count : 0,
        second_count : 0,
    }
    document.getElementById('post_list').innerHTML = '';                //清空訊息欄
    document.getElementById('room_name').innerHTML = theName;           //改房間名
    
    // room_base.postsRef.off();
    room_base.postsRef.once('value').then(function(snapshot) {          //把新房間的舊訊息印出
        snapshot.forEach(function(childshot) {
            var childData = childshot.val();
            var count = 1;
            for(i=0;i<childData.data.length;i++){
                if(childData.data[i]=="\n") count++;
                // console.log(count);
            }
            room_base.total_post[room_base.total_post.length] = str_before_username + childData.name +" (" + childData.email + ")" +"</strong><textarea rows=" + count + " readonly class='form-control-plaintext'>"+ childData.data + "</textarea>" + str_after_content;
            room_base.first_count += 1
        });

        document.getElementById('post_list').innerHTML = room_base.total_post.join('');         //把組合好的html語言插入訊息欄區

        room_base.postsRef.on('child_added', function(data) {
            room_base.second_count += 1;
            if (room_base.second_count > room_base.first_count) {
                setTimeout(function() {
                    //your code to be executed after 1 second
                    console.log(notmymes);
                    var childData = data.val();
                    var count = 1;
                    for(i=0;i<childData.data.length;i++){
                        if(childData.data[i]=="\n") count++;
                        // console.log(count);
                    }
                    room_base.total_post[room_base.total_post.length] = str_before_username + childData.name +" (" + childData.email + ")" + "</strong><textarea rows=" + count + " readonly class='form-control-plaintext'>"+ childData.data + "</textarea>" + str_after_content;
                    document.getElementById('post_list').innerHTML = room_base.total_post.join('');
                    console.log(notmymes,'0')
                    if(notmymes===1){
                        notifyMe(childData.name +' 在 ' +theName+ ' 裡說:\n' + childData.data);
                        console.log(notmymes,'1');
                    }
                    notmymes = 1;
                    console.log(notmymes,'2');
                }, 10);
                
                
            }
            
        });
    })
    .catch(e => console.log(e.message));

}


function notifyMe(note) {
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
      alert("This browser does not support desktop notification");
    }
  
    // Let's check if the user is okay to get some notification
    else if (Notification.permission === "granted") {
      // If it's okay let's create a notification
      var notification = new Notification(note);
    }
  
    // Otherwise, we need to ask the user for permission
    // Note, Chrome does not implement the permission static property
    // So we have to check for NOT 'denied' instead of 'default'
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
  
        // Whatever the user answers, we make sure we store the information
        if(!('permission' in Notification)) {
          Notification.permission = permission;
        }
  
        // If the user is okay, let's create a notification
        if (permission === "granted") {
          var notification = new Notification(note);
        }
      });
    } else {
      alert(`Permission is ${Notification.permission}`);
    }
  }